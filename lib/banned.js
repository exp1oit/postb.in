module.exports = {

  // TEST (Development)
  // "::ffff:127.0.0.1": true,

  // 34.207.71.212 - - [17/May/2019:09:44:07 +0000] "POST /2aV47kPP HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '34.207.71.212': true,

  // ???
  '67.160.11.242': true,

  // 54.68.12.169 - - [17/May/2019:09:42:47 +0000] "GET /B5mtfJsk HTTP/1.0" 404 16 "-" "FreshpingBot/1.0 (+https://freshping.io/)"
  '54.68.12.169': true,
  // 35.155.213.162 - - [17/May/2019:10:08:35 +0000] "POST /LCkGoV0k HTTP/1.0" 404 16 "-" "FreshpingWebhookSender/1.0 (+https://freshping.io/)"
  '35.155.213.162': true,
  // 52.42.211.55 - - [17/May/2019:10:53:25 +0000] "POST /LCkGoV0k HTTP/1.0" 404 16 "-" "FreshpingWebhookSender/1.0 (+https://freshping.io/)"
  '52.42.211.55': true,

  // 52.90.183.220 - - [17/May/2019:09:44:07 +0000] "POST /2aV47kPP HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '52.90.183.220': true,

  // 34.239.166.161 - - [17/May/2019:09:45:08 +0000] "POST /ocs6FVFX HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '34.239.166.161': true,
  // 34.200.245.8 - - [17/May/2019:10:10:25 +0000] "POST /BUOSo2OQ HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '34.200.245.8': true,

  // 54.167.65.32 - - [17/May/2019:10:10:24 +0000] "POST /2aV47kPP HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '54.167.65.32': true,
  // 54.198.122.135 - - [17/May/2019:10:10:25 +0000] "POST /ocs6FVFX HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '54.198.122.135': true,
  // 100.26.176.17 - - [17/May/2019:10:13:58 +0000] "POST /yvsGyask HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '100.26.176.17': true,

  // 204.141.42.226 - - [17/May/2019:09:47:02 +0000] "POST /k58ZbdlO HTTP/1.0" 500 28 "-" "Site 24 X 7"
  // sending incorrect JSON, hence the 500
  '204.141.42.226': true,

  // 121.244.91.2 - - [17/May/2019:09:59:23 +0000] "POST /XebUpN50 HTTP/1.0" 404 16 "-" "Site 24 X 7"
  '121.244.91.2': true,

  // Recurly? Interesting how people just leave these things on.
  // 52.203.102.94 - endpoint [17/May/2019:09:48:19 +0000] "POST /QQshKFx9 HTTP/1.0" 404 16 "-" "Recurly Webhooks/2.0 (+https://docs.recurly.com/push-notifications)"
  '52.203.102.94': true,
  // 35.192.147.126 - - [17/May/2019:09:58:55 +0000] "POST /YJhFczjO HTTP/1.0" 404 16 "-" "Recurly Webhooks/2.0 (+https://docs.recurly.com/push-notifications)"
  '35.192.147.126': true,

  // 88.99.168.190 - - [17/May/2019:09:50:01 +0000] "GET /fYE4rbUr?hooktype=PAYIN_FAILED&resourceid=10184A7FJKPR7LR2TRYJ HTTP/1.0" 404 16 "-" "Apache-HttpClient/4.5.4 (Java/1.8.0_151)"
  '88.99.168.190': true,
  // 51.4.145.210 - - [17/May/2019:09:50:00 +0000] "GET /MK3N0CuG?hooktype=PAYIN_FAILED&resourceid=0818SRQMKVSB95X3WH9E HTTP/1.0" 404 16 "-" "Apache-HttpClient/4.5.4 (Java/1.8.0_201)"
  '51.4.145.210': true,

  // 3.89.67.121 - - [17/May/2019:09:50:04 +0000] "POST /BUOSo2OQ HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '3.89.67.121': true,
  // 3.88.240.121 - - [17/May/2019:10:14:45 +0000] "POST /BUOSo2OQ HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '3.88.240.121': true,

  // 18.207.149.148 - - [17/May/2019:09:51:21 +0000] "POST /yvsGyask HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '18.207.149.148': true,

  // 3.214.216.90 - - [17/May/2019:09:54:41 +0000] "POST /ocs6FVFX HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '3.214.216.90': true,

  // 34.204.203.16 - - [17/May/2019:09:54:42 +0000] "POST /BUOSo2OQ HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '34.204.203.16': true,

  // 3.86.46.168 - - [17/May/2019:10:00:16 +0000] "POST /BUOSo2OQ HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '3.86.46.168': true,
  // 3.91.145.7 - - [17/May/2019:10:03:37 +0000] "POST /yvsGyask HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '3.91.145.7': true,

  // 52.21.118.105 - - [17/May/2019:10:09:07 +0000] "POST /b/FZR2sQEF HTTP/1.0" 404 16 "-" "heap-webhook/1.0"
  '52.21.118.105': true,

  // 52.169.225.45 - - [17/May/2019:10:17:09 +0000] "POST /b/aPvl2OBE HTTP/1.0" 404 16 "-" "http-ttn/2.6.0"
  '52.169.225.45': true,

  // 123.56.73.124 - - [17/May/2019:10:17:33 +0000] "POST /BJ8sR1cJ?user_id=yuzhou&user_key=yuzhou HTTP/1.0" 404 16 "-" "Atlassian HttpClient 1.0.1 / JIRA-7.13.0 (713000) / Default"
  '123.56.73.124': true,

  // 3.214.217.247 - - [17/May/2019:10:40:43 +0000] "POST /yvsGyask HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '3.214.217.247': true,

  // 63.148.46.2 - - [17/May/2019:10:41:15 +0000] "GET /Dju4Grf5 HTTP/1.0" 404 16 "-" "-"
  '63.148.46.2': true,

  // 35.172.216.122 - - [17/May/2019:10:42:54 +0000] "POST /BUOSo2OQ HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '35.172.216.122': true,

  // 68.170.26.253 - - [17/May/2019:11:24:53 +0000] "POST /BiPhLZuz HTTP/1.0" 404 16 "-" "Java/1.8.0_181"
  '68.170.26.253': true,

  // 54.144.119.109 - - [17/May/2019:11:24:53 +0000] "POST /2aV47kPP HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '54.144.119.109': true,

  // 35.225.96.112 - - [17/May/2019:11:36:52 +0000] "POST /I66qemiu HTTP/1.0" 404 16 "-" "Ruby"
  '35.225.96.112': true,

  // 104.198.183.22 - - [17/May/2019:11:36:52 +0000] "POST /I66qemiu HTTP/1.0" 404 16 "-" "Ruby"
  '104.198.183.22': true,

  // 3.82.3.191 - - [17/May/2019:11:51:50 +0000] "POST /yvsGyask HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '3.82.3.191': true,

  // 54.82.163.153 - - [17/May/2019:11:51:50 +0000] "POST /ocs6FVFX HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '54.82.163.153': true,

  // 54.84.187.39 - - [17/May/2019:11:54:34 +0000] "POST /yvsGyask HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '54.84.187.39': true,

  // 34.204.2.164 - - [17/May/2019:11:54:34 +0000] "POST /BUOSo2OQ HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '34.204.2.164': true,

  // 18.213.247.76 - - [17/May/2019:11:54:31 +0000] "POST /ocs6FVFX HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '18.213.247.76': true,

  // 191.233.102.89 - - [17/May/2019:12:01:32 +0000] "POST /b/RNYntHUG HTTP/1.0" 404 16 "-" "-"
  '191.233.102.89': true,

  // 34.201.32.155 - - [17/May/2019:12:01:32 +0000] "POST /yvsGyask HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '34.201.32.155': true,

  // 34.236.155.42 - - [17/May/2019:12:03:24 +0000] "POST /yvsGyask HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '34.236.155.42': true,

  // 52.38.141.91 - - [17/May/2019:12:03:24 +0000] "POST /460JoC9Z HTTP/1.0" 404 16 "-" "python-requests/2.9.1"
  '52.38.141.91': true,

  // 52.0.253.247 - - [17/May/2019:12:03:24 +0000] "GET /oFfRCcFW HTTP/1.0" 404 16 "-" "Jetty/9.2.10.v20150310"
  '52.0.253.247': true,

  // 52.49.21.251 - - [17/May/2019:12:06:04 +0000] "GET /EMcK5Aq1 HTTP/1.0" 404 16 "-" "http-kit/2.0"
  '52.49.21.251': true,

  // 18.185.25.26 - - [17/May/2019:12:06:03 +0000] "POST /mdxYqN40 HTTP/1.0" 404 16 "-" "Apache-HttpClient/4.5.7 (Java/11.0.3)"
  '18.185.25.26': true,

  // 3.90.139.224 - - [17/May/2019:20:10:31 +0000] "POST /BUOSo2OQ HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '3.90.139.224': true,

  // 34.230.45.250 - - [17/May/2019:20:10:30 +0000] "POST /yvsGyask HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '34.230.45.250': true,

  // 3.91.16.223 - - [17/May/2019:20:10:30 +0000] "POST /ocs6FVFX HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '3.91.16.223': true,

  // 34.236.255.201 - - [17/May/2019:20:10:30 +0000] "POST /2aV47kPP HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '34.236.255.201': true,

  // 23.22.180.96 - - [17/May/2019:20:10:29 +0000] "POST /VY8jj8f1 HTTP/1.0" 404 16 "-" "-"
  '23.22.180.96': true,

  // 3.216.78.229 - - [18/May/2019:10:12:21 +0000] "POST /BUOSo2OQ HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '3.216.78.229': true,

  // 3.84.184.30 - - [18/May/2019:10:12:21 +0000] "POST /yvsGyask HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '3.84.184.30': true,

  // 54.210.95.163 - - [18/May/2019:10:12:21 +0000] "POST /ocs6FVFX HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '54.210.95.163': true,

  // 34.229.229.176 - - [18/May/2019:10:12:20 +0000] "POST /2aV47kPP HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '34.229.229.176': true,

  // 54.197.222.52 - - [18/May/2019:10:13:58 +0000] "POST /yvsGyask HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '54.197.222.52': true,

  // 18.213.246.22 - - [18/May/2019:11:40:08 +0000] "POST /ocs6FVFX HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '18.213.246.22': true,

  // 34.229.241.178 - - [18/May/2019:11:40:07 +0000] "POST /BUOSo2OQ HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '34.229.241.178': true,

  // 35.175.153.139 - - [18/May/2019:11:40:07 +0000] "POST /yvsGyask HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '35.175.153.139': true,

  // 35.172.217.80 - - [18/May/2019:11:40:07 +0000] "POST /2aV47kPP HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '35.172.217.80': true,

  // 52.0.253.251 - - [18/May/2019:11:40:07 +0000] "GET /oFfRCcFW HTTP/1.0" 404 16 "-" "Jetty/9.2.10.v20150310"
  '52.0.253.251': true,

  // 18.204.37.252 - - [18/May/2019:11:42:59 +0000] "POST /BUOSo2OQ HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '18.204.37.252': true,

  // 35.172.224.106 - - [18/May/2019:11:42:58 +0000] "POST /2aV47kPP HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '35.172.224.106': true,

  // 52.5.160.108 - - [18/May/2019:11:42:57 +0000] "POST /b/GhjlMpmi HTTP/1.0" 404 16 "-" "python-requests/2.21.0"
  '52.5.160.108': true,

  // 35.172.230.248 - - [18/May/2019:11:42:56 +0000] "POST /ocs6FVFX HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '35.172.230.248': true,

  // 3.208.9.99 - - [18/May/2019:11:42:56 +0000] "POST /ocs6FVFX HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '3.208.9.99': true,

  // 52.193.45.108 - - [19/May/2019:09:01:04 +0000] "POST /Z3beRdXg HTTP/1.0" 404 16 "-" "Java/1.8.0_181"
  '52.193.45.108': true,

  // 3.83.148.22 - - [19/May/2019:09:01:04 +0000] "POST /2aV47kPP HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '3.83.148.22': true,

  // 3.80.24.78 - - [19/May/2019:09:01:04 +0000] "POST /ocs6FVFX HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '3.80.24.78': true,

  // 3.80.159.63 - - [19/May/2019:09:01:04 +0000] "POST /yvsGyask HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '3.80.159.63': true,

  // 3.95.160.189 - - [19/May/2019:23:04:31 +0000] "POST /ocs6FVFX HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '3.95.160.189': true,

  // 54.160.159.200 - - [19/May/2019:23:04:58 +0000] "POST /yvsGyask HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '54.160.159.200': true,

  // 34.201.103.110 - - [19/May/2019:23:05:14 +0000] "POST /2aV47kPP HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '34.201.103.110': true,

  // 52.71.119.89 - - [19/May/2019:23:05:33 +0000] "POST /b/GhjlMpmi HTTP/1.0" 404 16 "-" "python-requests/2.21.0"
  '52.71.119.89': true,

  // 3.80.72.53 - - [20/May/2019:00:45:07 +0000] "POST /BUOSo2OQ HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '3.80.72.53': true,

  // 110.5.116.203 - - [20/May/2019:00:46:19 +0000] "POST /5EhoPwJV HTTP/1.0" 404 16 "https://postb.in/5EhoPwJV" "php-requests/1.7"
  '110.5.116.203': true,

  // 107.178.216.208 - - [20/May/2019:00:46:37 +0000] "POST /rbVf3CJE?dblock_keymr=c9733261b4263f189148e1f8b54777903b7b82a42f193ed533697206b7e1fa60&stage=factom HTTP/1.0" 404 16 "-" "hackney/1.14.3"
  '107.178.216.208': true,

  // 173.0.81.124 - - [20/May/2019:00:46:59 +0000] "POST /b/eEaWvww8 HTTP/1.0" 404 16 "-" "PayPal/AUHR-214.0-51787073"
  '173.0.81.124': true,

  // 35.224.216.141 - - [20/May/2019:00:47:18 +0000] "POST /5fdmQhoU HTTP/1.0" 404 16 "-" "Instapage Webhook"
  '35.224.216.141': true,

  // 100.26.251.219 - - [20/May/2019:00:50:16 +0000] "POST /ocs6FVFX HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '100.26.251.219': true,

  // 52.87.168.218 - - [20/May/2019:00:50:17 +0000] "POST /yvsGyask HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '52.87.168.218': true,

  // 54.236.243.253 - - [20/May/2019:00:51:50 +0000] "POST /yvsGyask HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '54.236.243.253': true,

  // 54.196.143.92 - - [20/May/2019:00:51:50 +0000] "POST /BUOSo2OQ HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '54.196.143.92': true,

  // 18.215.187.236 - - [20/May/2019:00:51:50 +0000] "POST /ocs6FVFX HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '18.215.187.236': true,

  // 34.200.247.200 - - [20/May/2019:00:53:44 +0000] "POST /2aV47kPP HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '34.200.247.200': true,

  // 52.0.253.244 - - [20/May/2019:00:54:06 +0000] "GET /oFfRCcFW HTTP/1.0" 404 16 "-" "Jetty/9.2.10.v20150310"
  '52.0.253.244': true,

  // 3.91.170.172 - - [20/May/2019:00:54:30 +0000] "POST /yvsGyask HTTP/1.0" 404 16 "-" "node-superagent/5.0.5"
  '3.91.170.172': true,

  // 52.57.5.24 - - [20/May/2019:01:39:18 +0000] "POST /1558235120051-5001565171405 HTTP/1.0" 404 16 "-" "Apache-HttpClient/4.5.7 (Java/11.0.3)"
  '52.57.5.24': true,

  // 8.40.89.184 - - [20/May/2019:19:07:35 +0000] "POST /QKdb7qPh HTTP/1.0" 403 74 "-" "Recurly Webhooks/2.0 (+https://docs.recurly.com/push-notifications)"
  '8.40.89.184': true,

  // 35.162.39.124 - - [20/May/2019:19:08:34 +0000] "GET /qo9f1Tpr HTTP/1.0" 403 74 "-" "Workfront Event Subscription"
  '35.162.39.124': true,

  // 91.89.69.207 - - [20/May/2019:19:08:56 +0000] "POST /b3sBSGhI HTTP/1.0" 403 74 "-" "Go-http-client/1.1"
  '91.89.69.207': true,

  // 34.250.48.186 - - [20/May/2019:19:09:18 +0000] "GET /EMcK5Aq1 HTTP/1.0" 403 74 "-" "http-kit/2.0"
  '34.250.48.186': true,

  // 54.145.95.26 - - [20/May/2019:19:10:05 +0000] "POST /yvsGyask HTTP/1.0" 403 74 "-" "node-superagent/5.0.5"
  '54.145.95.26': true,

  // 35.173.69.250 - - [20/May/2019:19:11:19 +0000] "POST /b/xHpcCObj HTTP/1.0" 404 16 "-" "hackney/1.8.6"
  '35.173.69.250': true,

  // 52.17.159.13 - - [20/May/2019:19:11:30 +0000] "GET /EMcK5Aq1 HTTP/1.0" 403 74 "-" "http-kit/2.0"
  '52.17.159.13': true

}
