// --------------------------------------------------------------------------------------------------------------------

'use strict'

// npm
const test = require('tape')
const request = require('request')

// --------------------------------------------------------------------------------------------------------------------

const PORT = process.env.PORT || 33515

// const baseUrl = 'http://postb.in'
const baseUrl = `http://localhost:${PORT}`

let binId

test('creating a bin', function (t) {
  t.plan(1)

  // firstly, make a bin
  request.post(baseUrl + '/api/bin', function (err, res, body) {
    t.equal(err, null, 'There was no error when creating the bin')

    // save this binId for subsequent requests
    body = JSON.parse(body)
    binId = body.binId

    t.end()
  })
})

test('delete this bin', function (t) {
  t.plan(1)

  request.del(baseUrl + '/api/bin/' + binId, function (err, res, body) {
    t.equal(err, null, 'There was no error with this request')

    body = JSON.parse(body)

    t.end()
  })
})

// --------------------------------------------------------------------------------------------------------------------
